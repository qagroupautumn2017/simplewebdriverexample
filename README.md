# Lecture for manual testers

Prerequisites:
Java 8 (jdk1.8) and Maven have to be installed on your system.


To download drivers, run

`
mvn driver-binary-downloader:selenium
`

It will download webdrivers into `selenium_standalone_binaries` folder within your project.

Optionally, one may download the chromedriver manually and place it by the project-directory relative path \\selenium_standalone_binaries\\windows\\googlechrome\\64bit\\chromedriver.exe

The test class is `LocalHtmlFileInBrowserTest` and placed in `/src/test/java/com/qagroup/start` folder.

Have a fun learning!



